//
//  HomeViewController.swift
//  Matrix Management
//
//  Created by NetPyx on 24/04/17.
//  Copyright © 2017 NetPyx. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    var contents:NSMutableArray = []
    var selectedRows:NSMutableArray = []
    var fruitsList:NSDictionary = [String : AnyObject]()
    var billInfoList:NSDictionary = [String : AnyObject]()
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblInvName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myString:NSString = String(format: "Welcome,%@", (NSUserDefaults.standardUserDefaults().valueForKey("User_Name") as? String)!)
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: nil)
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.blackColor(), range: NSRange(location:0,length:7))
        // set label Attribute
        lblUsername.attributedText = myMutableString
        
        
        startActivityIndicator("Getting you in..")
        FetchHomeData()
        fetchDocDir()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Tableview Delegate & Datasource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    // number of rows in table view
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView != tblView
        {
            if ((self.billInfoList.objectForKey("data")?.count) != nil)
            {
                if (self.billInfoList.objectForKey("data")?.objectForKey("inv_discount_item"))! as? String == "0"
                {
                    return (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)! + 4
                }
                return (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)! + 5
            }
            return 0
        }
        else
        {
            if ((self.fruitsList.objectForKey("data")?.count) != nil) {
                return (self.fruitsList.objectForKey("data")?.count)!
            }
            return 0
        }
    }
    
    // create a cell for each table view row
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if tableView != tblView
        {
            if indexPath.row == 0
            {
                // create a new cell if needed or reuse an old one
                let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("HeaderCell") as UITableViewCell!
                
                let headerView = cell.viewWithTag(12) as UIView!
                let lblBillTo = headerView!.viewWithTag(101) as! UILabel
                let lblBillDate = headerView!.viewWithTag(102) as! UILabel
                let lblBillFor = headerView!.viewWithTag(103) as! UILabel
                let lblBillNumber = headerView!.viewWithTag(104) as! UILabel
                
                lblBillTo.text = (self.billInfoList.objectForKey("data")?.objectForKey("bill_to"))! as? String
                lblBillDate.text = (self.billInfoList.objectForKey("data")?.objectForKey("inv_date"))! as? String
                lblBillFor.text = (self.billInfoList.objectForKey("data")?.objectForKey("inv_billed_for"))! as? String
                lblBillNumber.text = (self.billInfoList.objectForKey("data")?.objectForKey("invoice_number"))! as? String
                
                return cell
            }
            else
            {
                // create a new cell if needed or reuse an old one
                let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("BillCell") as UITableViewCell!
                let lblItemName = cell.viewWithTag(201) as! UILabel
                let lblItemQuantity = cell.viewWithTag(202) as! UILabel
                let lblItemUnitPrice = cell.viewWithTag(203) as! UILabel
                let lblItemTotalPrice = cell.viewWithTag(204) as! UILabel
                let headerView = cell.viewWithTag(205) as UIView!
                let lblItemDiscount = cell.viewWithTag(206) as! UILabel
                
                if indexPath.row <=  (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)
                {
                    if indexPath.row == 1
                    {
                        headerView.hidden = false
                        if lblItemName.frame.origin.y < headerView.frame.size.height
                        {
                            lblItemName.frame = CGRectMake(lblItemName.frame.origin.x, lblItemName.frame.origin.y+headerView.frame.size.height, lblItemName.frame.size.width, lblItemName.frame.size.height)
                            lblItemQuantity.frame = CGRectMake(lblItemQuantity.frame.origin.x, lblItemQuantity.frame.origin.y+headerView.frame.size.height, lblItemQuantity.frame.size.width, lblItemQuantity.frame.size.height)
                            lblItemUnitPrice.frame = CGRectMake(lblItemUnitPrice.frame.origin.x, lblItemUnitPrice.frame.origin.y+headerView.frame.size.height, lblItemUnitPrice.frame.size.width, lblItemUnitPrice.frame.size.height)
                            lblItemTotalPrice.frame = CGRectMake(lblItemTotalPrice.frame.origin.x, lblItemTotalPrice.frame.origin.y+headerView.frame.size.height, lblItemTotalPrice.frame.size.width, lblItemTotalPrice.frame.size.height)
                            lblItemDiscount.frame = CGRectMake(lblItemDiscount.frame.origin.x, lblItemDiscount.frame.origin.y+headerView.frame.size.height, lblItemDiscount.frame.size.width, lblItemDiscount.frame.size.height)
                        }
                        lblItemName.text = (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("description"))! as? String
                        lblItemQuantity.text = (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("quantity"))! as? String
                        lblItemUnitPrice.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("item_rate"))! as? String)!)
                        lblItemTotalPrice.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("total"))! as? String)!)
                        lblItemDiscount.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("discount"))! as? String)!)
                    }
                    else
                    {
                        headerView.hidden = true
                        if lblItemName.frame.origin.y > headerView.frame.size.height
                        {
                            lblItemName.frame = CGRectMake(lblItemName.frame.origin.x, lblItemName.frame.origin.y-headerView.frame.size.height, lblItemName.frame.size.width, lblItemName.frame.size.height)
                            lblItemQuantity.frame = CGRectMake(lblItemQuantity.frame.origin.x, lblItemQuantity.frame.origin.y-headerView.frame.size.height, lblItemQuantity.frame.size.width, lblItemQuantity.frame.size.height)
                            lblItemUnitPrice.frame = CGRectMake(lblItemUnitPrice.frame.origin.x, lblItemUnitPrice.frame.origin.y-headerView.frame.size.height, lblItemUnitPrice.frame.size.width, lblItemUnitPrice.frame.size.height)
                            lblItemTotalPrice.frame = CGRectMake(lblItemTotalPrice.frame.origin.x, lblItemTotalPrice.frame.origin.y-headerView.frame.size.height, lblItemTotalPrice.frame.size.width, lblItemTotalPrice.frame.size.height)
                            lblItemDiscount.frame = CGRectMake(lblItemDiscount.frame.origin.x, lblItemDiscount.frame.origin.y-headerView.frame.size.height, lblItemDiscount.frame.size.width, lblItemDiscount.frame.size.height)
                        }
                        
                        lblItemName.text = (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("description"))! as? String
                        lblItemQuantity.text = (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("quantity"))! as? String
                        lblItemUnitPrice.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("item_rate"))! as? String)!)
                        lblItemTotalPrice.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("total"))! as? String)!)
                        lblItemDiscount.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.objectAtIndex(indexPath.row-1).objectForKey("discount"))! as? String)!)
                        
                    }
                }
                
                
                if indexPath.row == (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)!+1
                {
                    headerView.hidden = true
                    if lblItemName.frame.origin.y > headerView.frame.size.height
                    {
                        lblItemName.frame = CGRectMake(lblItemName.frame.origin.x, lblItemName.frame.origin.y-headerView.frame.size.height, lblItemName.frame.size.width, lblItemName.frame.size.height)
                        lblItemQuantity.frame = CGRectMake(lblItemQuantity.frame.origin.x, lblItemQuantity.frame.origin.y-headerView.frame.size.height, lblItemQuantity.frame.size.width, lblItemQuantity.frame.size.height)
                        lblItemUnitPrice.frame = CGRectMake(lblItemUnitPrice.frame.origin.x, lblItemUnitPrice.frame.origin.y-headerView.frame.size.height, lblItemUnitPrice.frame.size.width, lblItemUnitPrice.frame.size.height)
                        lblItemTotalPrice.frame = CGRectMake(lblItemTotalPrice.frame.origin.x, lblItemTotalPrice.frame.origin.y-headerView.frame.size.height, lblItemTotalPrice.frame.size.width, lblItemTotalPrice.frame.size.height)
                        lblItemDiscount.frame = CGRectMake(lblItemDiscount.frame.origin.x, lblItemDiscount.frame.origin.y-headerView.frame.size.height, lblItemDiscount.frame.size.width, lblItemDiscount.frame.size.height)
                    }
                    
                    lblItemName.hidden = true
                    lblItemQuantity.hidden = true
                    lblItemDiscount.hidden = true
                    lblItemUnitPrice.text = "Sub Total"
                    lblItemTotalPrice.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("inv_subtotal"))! as? String)!)
                    
                    cell.contentView.backgroundColor = UIColor(red: 230, green: 237, blue: 239, alpha: 1.0)
                    
                }
                
                if indexPath.row == (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)!+2
                {
                    headerView.hidden = true
                    if lblItemName.frame.origin.y > headerView.frame.size.height
                    {
                        lblItemName.frame = CGRectMake(lblItemName.frame.origin.x, lblItemName.frame.origin.y-headerView.frame.size.height, lblItemName.frame.size.width, lblItemName.frame.size.height)
                        lblItemQuantity.frame = CGRectMake(lblItemQuantity.frame.origin.x, lblItemQuantity.frame.origin.y-headerView.frame.size.height, lblItemQuantity.frame.size.width, lblItemQuantity.frame.size.height)
                        lblItemUnitPrice.frame = CGRectMake(lblItemUnitPrice.frame.origin.x, lblItemUnitPrice.frame.origin.y-headerView.frame.size.height, lblItemUnitPrice.frame.size.width, lblItemUnitPrice.frame.size.height)
                        lblItemTotalPrice.frame = CGRectMake(lblItemTotalPrice.frame.origin.x, lblItemTotalPrice.frame.origin.y-headerView.frame.size.height, lblItemTotalPrice.frame.size.width, lblItemTotalPrice.frame.size.height)
                        lblItemDiscount.frame = CGRectMake(lblItemDiscount.frame.origin.x, lblItemDiscount.frame.origin.y-headerView.frame.size.height, lblItemDiscount.frame.size.width, lblItemDiscount.frame.size.height)
                    }
                    
                    lblItemName.hidden = true
                    lblItemQuantity.hidden = true
                    lblItemDiscount.hidden = true
                    lblItemUnitPrice.text = "Paypal Fees"
                    lblItemTotalPrice.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("paypal_fee"))! as? String)!)
                    
                    cell.contentView.backgroundColor = UIColor(red: 230, green: 237, blue: 239, alpha: 1.0)
                }
                
                if indexPath.row == (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)!+3
                {
                    headerView.hidden = true
                    if lblItemName.frame.origin.y > headerView.frame.size.height
                    {
                        lblItemName.frame = CGRectMake(lblItemName.frame.origin.x, lblItemName.frame.origin.y-headerView.frame.size.height, lblItemName.frame.size.width, lblItemName.frame.size.height)
                        lblItemQuantity.frame = CGRectMake(lblItemQuantity.frame.origin.x, lblItemQuantity.frame.origin.y-headerView.frame.size.height, lblItemQuantity.frame.size.width, lblItemQuantity.frame.size.height)
                        lblItemUnitPrice.frame = CGRectMake(lblItemUnitPrice.frame.origin.x, lblItemUnitPrice.frame.origin.y-headerView.frame.size.height, lblItemUnitPrice.frame.size.width, lblItemUnitPrice.frame.size.height)
                        lblItemTotalPrice.frame = CGRectMake(lblItemTotalPrice.frame.origin.x, lblItemTotalPrice.frame.origin.y-headerView.frame.size.height, lblItemTotalPrice.frame.size.width, lblItemTotalPrice.frame.size.height)
                        lblItemDiscount.frame = CGRectMake(lblItemDiscount.frame.origin.x, lblItemDiscount.frame.origin.y-headerView.frame.size.height, lblItemDiscount.frame.size.width, lblItemDiscount.frame.size.height)
                    }
                    
                    if (self.billInfoList.objectForKey("data")?.objectForKey("inv_discount_item"))! as? String == "0"
                    {
                        lblItemName.hidden = false
                        lblItemQuantity.hidden = true
                        lblItemDiscount.hidden = true
                        lblItemUnitPrice.text = "Grand Total"
                        lblItemTotalPrice.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("inv_grandtotal"))! as? String)!)
                        lblItemName.text = (self.billInfoList.objectForKey("data")?.objectForKey("grand_total_in_words"))! as? String
                        lblItemName.frame = CGRectMake(lblItemName.frame.origin.x, lblItemName.frame.origin.y, lblItemName.frame.size.width+50, lblItemName.frame.size.height)
                    }
                    else
                    {
                        lblItemName.hidden = true
                        lblItemQuantity.hidden = true
                        lblItemDiscount.hidden = true
                        lblItemUnitPrice.text = "Discount"
                        lblItemTotalPrice.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("inv_granddiscount"))! as? String)!)
                    }
                }
                
                if indexPath.row == (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)!+4
                {
                    headerView.hidden = true
                    if lblItemName.frame.origin.y > headerView.frame.size.height
                    {
                        lblItemName.frame = CGRectMake(lblItemName.frame.origin.x, lblItemName.frame.origin.y-headerView.frame.size.height, lblItemName.frame.size.width, lblItemName.frame.size.height)
                        lblItemQuantity.frame = CGRectMake(lblItemQuantity.frame.origin.x, lblItemQuantity.frame.origin.y-headerView.frame.size.height, lblItemQuantity.frame.size.width, lblItemQuantity.frame.size.height)
                        lblItemUnitPrice.frame = CGRectMake(lblItemUnitPrice.frame.origin.x, lblItemUnitPrice.frame.origin.y-headerView.frame.size.height, lblItemUnitPrice.frame.size.width, lblItemUnitPrice.frame.size.height)
                        lblItemTotalPrice.frame = CGRectMake(lblItemTotalPrice.frame.origin.x, lblItemTotalPrice.frame.origin.y-headerView.frame.size.height, lblItemTotalPrice.frame.size.width, lblItemTotalPrice.frame.size.height)
                        lblItemDiscount.frame = CGRectMake(lblItemDiscount.frame.origin.x, lblItemDiscount.frame.origin.y-headerView.frame.size.height, lblItemDiscount.frame.size.width, lblItemDiscount.frame.size.height)
                    }
                    
                    lblItemName.hidden = false
                    lblItemQuantity.hidden = true
                    lblItemDiscount.hidden = true
                    lblItemUnitPrice.text = "Grand Total"
                    lblItemTotalPrice.text = String(format: "%@ %@",((self.billInfoList.objectForKey("data")?.objectForKey("currency_code"))! as? String)!,((self.billInfoList.objectForKey("data")?.objectForKey("inv_grandtotal"))! as? String)!)
                    lblItemName.text = (self.billInfoList.objectForKey("data")?.objectForKey("grand_total_in_words"))! as? String
                    lblItemName.frame = CGRectMake(lblItemName.frame.origin.x, lblItemName.frame.origin.y, lblItemName.frame.size.width+50, lblItemName.frame.size.height)
                }
                lblItemName.sizeToFit()
                return cell
            }
        }
        else
        {
            // create a new cell if needed or reuse an old one
            let cell:UITableViewCell = tblView.dequeueReusableCellWithIdentifier("Cell") as UITableViewCell!
            
            if indexPath.row%2 == 0 {
                cell.contentView.backgroundColor = UIColor.whiteColor()
            }
            
            // change background color of selected cell
            let bgColorView = UIView()
            bgColorView.backgroundColor = UIColor.clearColor()
            cell.selectedBackgroundView = bgColorView
            
            // set the text from the data model
            let lblName = cell.viewWithTag(1) as! UILabel
            let lblRate = cell.viewWithTag(2) as! UILabel
            let lblNumber = cell.viewWithTag(3) as! UILabel
            let btnDownload = cell.viewWithTag(4) as! UIButton
            let imgPlus = cell.viewWithTag(5) as! UIImageView
            let imgDownload = cell.viewWithTag(6) as! UIImageView
            let lblDownload = cell.viewWithTag(7) as! UILabel
            let activityIndicator = cell.viewWithTag(8) as! UIActivityIndicatorView
            
            activityIndicator.hidden = true
            var tempPartyName = (self.fruitsList.objectForKey("data")?.objectAtIndex(indexPath.row).objectForKey("party_company"))! as? String
            tempPartyName = tempPartyName?.stringByReplacingOccurrencesOfString("&amp;", withString: "&")
            lblName.text = tempPartyName?.uppercaseString
            
            lblRate.text = ((self.fruitsList.objectForKey("data")?.objectAtIndex(indexPath.row).objectForKey("grand_total"))! as? String)!
            lblNumber.text = (self.fruitsList.objectForKey("data")?.objectAtIndex(indexPath.row).objectForKey("invoice_number"))! as? String
            
            btnDownload.addTarget(self, action: #selector(btnActionDownload(_:)), forControlEvents: .TouchUpInside)
            
            if contents.containsObject((self.fruitsList.objectForKey("data")?.objectAtIndex(indexPath.row).objectForKey("inv_pdf_filename"))!)
            {
                lblDownload.text = "View"
                imgDownload.image = UIImage(named: "icon_view")
                if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                {
                    imgDownload.frame = CGRectMake(lblDownload.frame.origin.x+16, imgDownload.frame.origin.y, imgDownload.frame.size.width, imgDownload.frame.size.height)
                }
                else
                {
                    imgDownload.frame = CGRectMake(lblDownload.frame.origin.x+13, imgDownload.frame.origin.y, imgDownload.frame.size.width, imgDownload.frame.size.height)
                }
            }
            else
            {
                lblDownload.text = "Download"
                imgDownload.image = UIImage(named: "icon_download")
                if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                {
                    imgDownload.frame = CGRectMake(lblDownload.frame.origin.x-20, imgDownload.frame.origin.y, imgDownload.frame.size.width, imgDownload.frame.size.height)
                }
                else
                {
                    imgDownload.frame = CGRectMake(lblDownload.frame.origin.x-10, imgDownload.frame.origin.y, imgDownload.frame.size.width, imgDownload.frame.size.height)
                }
            }
            
            imgPlus.frame = CGRectMake(lblNumber.intrinsicContentSize().width+12, imgPlus.frame.origin.y, imgPlus.frame.size.width, imgPlus.frame.size.height)
            imgPlus.image = UIImage(named: "icon_plus")
            
            return cell
        }
    }
    
    // method to run when table view cell is tapped
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        self.tblView.reloadData()
        let cell:UITableViewCell = (tblView.cellForRowAtIndexPath(indexPath) as UITableViewCell?)!
        let billTblView = cell.viewWithTag(11) as! UITableView!
        let imgPlus = cell.viewWithTag(5) as! UIImageView
        let activityIndicator = cell.viewWithTag(8) as! UIActivityIndicatorView
        if tableView == tblView
        {
            if selectedRows.containsObject(indexPath.row)
            {
                let collapsedOptions: UIViewAnimationOptions = [.TransitionFlipFromBottom, .CurveEaseIn]
                self.selectedRows.removeAllObjects()
                UIView.animateWithDuration(0.5, delay: 0.1, options: collapsedOptions, animations:{ () -> Void in
                    imgPlus.image = UIImage(named: "icon_plus")
                    self.tblView.beginUpdates()
                    self.tblView.endUpdates()
                    
                    }, completion: { (finished) -> Void in
                })
            }
            else
            {
                let expandedOptions: UIViewAnimationOptions = [.TransitionFlipFromTop, .CurveEaseOut]
                UIView.animateWithDuration(0.5, delay: 0.1, options: expandedOptions, animations:{ () -> Void in
                    self.selectedRows.removeAllObjects()
                    self.selectedRows.addObject(indexPath.row)
                    
                    if ReachabilityManager.sharedInstance.isConnectedToNetwork()
                    {
                        activityIndicator.hidden = false
                        activityIndicator.startAnimating()
                        let session = NSURLSession.sharedSession()
                        let request = NSMutableURLRequest(URL: NSURL(string: globalUrl)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
                        
                        // create some JSON data and configure the request
                        let jsonString = String(format: "invoice_id=%@&api_type=get_order_data", ((self.fruitsList.objectForKey("data")?.objectAtIndex(indexPath.row).objectForKey("inv_id"))! as? String)!)
                        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
                        request.HTTPMethod = "POST"
                        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                        
                        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                            if let error = error {
                                print(error)
                                self.stopActivityIndicator()
                                let alert = UIAlertController(title: "Alert!", message: error.localizedDescription, preferredStyle:.Alert)
                                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                                    print(action)
                                }
                                alert.addAction(cancelAction)
                                self.presentViewController(alert, animated: true, completion: nil)
                            }
                            if let data = data{
                                print("data =\(data)")
                            }
                            if let response = response {
                                // print("url = \(response.URL!)")
                                print("response = \(response)")
                                // let httpResponse = response as! NSHTTPURLResponse
                                // print("response code = \(httpResponse.statusCode)")
                                
                                //if you response is json do the following
                                do{
                                    let resultJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())
                                    let arrayJSON = resultJSON as! NSDictionary
                                    
                                    if(arrayJSON.objectForKey("type") as! String == "success")
                                    {
                                        self.billInfoList = arrayJSON
                                        dispatch_async(dispatch_get_main_queue(),{
                                            activityIndicator.hidden = true
                                            activityIndicator.stopAnimating()
                                            imgPlus.image = UIImage(named: "icon_minus")
                                            billTblView.reloadData()
                                            self.tblView.beginUpdates()
                                            self.tblView.endUpdates()
                                            
                                        })
                                        return
                                    }
                                    else
                                    {
                                        dispatch_async(dispatch_get_main_queue(),{
                                            let alert = UIAlertController(title: "Alert!", message: arrayJSON.objectForKey("message") as? String, preferredStyle:.Alert)
                                            let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                                                print(action)
                                            }
                                            alert.addAction(cancelAction)
                                            self.presentViewController(alert, animated: true, completion: nil)
                                        })
                                    }
                                    
                                }catch _{
                                    print("Received not-well-formatted JSON")
                                }
                            }
                        })
                        task.resume()
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert!", message: "No Internet Connection.", preferredStyle:.Alert)
                        let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                        }
                        alert.addAction(cancelAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    }, completion: { (finished) -> Void in
                })
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if tableView != tblView
        {
            if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                if indexPath.row == 0
                {
                    return 140
                }
                if indexPath.row == 1
                {
                    return 80
                }
                return 60
            }
            else
            {
                if indexPath.row == 0
                {
                    return 125
                }
                if indexPath.row == 1
                {
                    return 60
                }
                return 30
            }
        }
        else
        {
            if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                if selectedRows.containsObject(indexPath.row)
                {
                    if ((self.billInfoList.objectForKey("data")?.count) != nil)
                    {
                        if (self.billInfoList.objectForKey("data")?.objectForKey("inv_discount_item"))! as? String == "0"
                        {
                            let rowH:NSInteger = (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)!+3
                            var rowHeight:CGFloat = CGFloat(rowH * 60)
                            rowHeight = rowHeight + 140 + 70 + 30
                            return rowHeight
                        }
                        else
                        {
                            let rowH:NSInteger = (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)!+4
                            var rowHeight:CGFloat = CGFloat(rowH * 60)
                            rowHeight = rowHeight + 140 + 70 + 30
                            return rowHeight
                        }
                    }
                    else
                    {
                        return 0
                    }
                }
                return 70.0
            }
            else
            {
                if selectedRows.containsObject(indexPath.row)
                {
                    if ((self.billInfoList.objectForKey("data")?.count) != nil)
                    {
                        if (self.billInfoList.objectForKey("data")?.objectForKey("inv_discount_item"))! as? String == "0"
                        {
                            let rowH:NSInteger = (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)!+3
                            var rowHeight:CGFloat = CGFloat(rowH * 30)
                            rowHeight = rowHeight + 125 + 50 + 40
                            return rowHeight
                        }
                        else
                        {
                            let rowH:NSInteger = (self.billInfoList.objectForKey("data")?.objectForKey("order_data")!.count)!+4
                            var rowHeight:CGFloat = CGFloat(rowH * 30)
                            rowHeight = rowHeight + 125 + 50 + 40
                            return rowHeight
                        }
                    }
                    else
                    {
                        return 0
                    }
                }
                return 50.0
            }
        }
    }

    //MARK: - Button Actions
    @IBAction func btnActionDone(sender: AnyObject)
    {
        UIView.animateWithDuration(1.0,
                                   delay: 0.0,
                                   options: .CurveEaseInOut,
                                   animations: {
                                    self.subView.frame = CGRectMake(self.subView.frame.origin.x, self.view.frame.size.height, self.subView.frame.size.width, self.subView.frame.size.height)
            },
                                   completion: { finished in
                                    self.subView.hidden = true
        })
    }
    
    @IBAction func btnActionLogout(sender: AnyObject)
    {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("Is_Login")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("User_Id")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("User_Name")
        
        for (controller) in self.navigationController!.viewControllers
        {
            if (controller.isKindOfClass(ViewController))
            {
                self.navigationController?.popToViewController(controller, animated: false)
                break;
            }
        }
        self.navigationController?.pushViewController(self.storyboard!.instantiateViewControllerWithIdentifier("LoginView") as UIViewController, animated: false)
    }
    
    
    //MARK: - Private Method
    func FetchHomeData()
    {
        if ReachabilityManager.sharedInstance.isConnectedToNetwork()
        {
            let session = NSURLSession.sharedSession()
            let request = NSMutableURLRequest(URL: NSURL(string: globalUrl)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
            
            // create some JSON data and configure the request
            let jsonString = String(format: "user_id=%@&api_type=get_sales_data", (NSUserDefaults.standardUserDefaults().valueForKey("User_Id") as? String)!)
            request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
            request.HTTPMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                if let error = error {
                    print(error)
                    self.stopActivityIndicator()
                    let alert = UIAlertController(title: "Alert!", message: error.localizedDescription, preferredStyle:.Alert)
                    let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                        print(action)
                    }
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                if let data = data{
                    print("data =\(data)")
                }
                if let response = response {
                    // print("url = \(response.URL!)")
                    print("response = \(response)")
                    // let httpResponse = response as! NSHTTPURLResponse
                    // print("response code = \(httpResponse.statusCode)")
                    
                    //if you response is json do the following
                    do{
                        let resultJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())
                        let arrayJSON = resultJSON as! NSDictionary
                        
                        if(arrayJSON.objectForKey("type") as! String == "success")
                        {
                            self.fruitsList = arrayJSON
                            dispatch_async(dispatch_get_main_queue(),{
                                self.tblView.reloadData()
                                self.stopActivityIndicator()
                            })
                            return
                        }
                        else
                        {
                            dispatch_async(dispatch_get_main_queue(),{
                                self.stopActivityIndicator()
                                let alert = UIAlertController(title: "Alert!", message: arrayJSON.objectForKey("message") as? String, preferredStyle:.Alert)
                                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                                    print(action)
                                }
                                alert.addAction(cancelAction)
                                self.presentViewController(alert, animated: true, completion: nil)
                            })
                        }
                        
                        
                    }catch _{
                        print("Received not-well-formatted JSON")
                    }
                }
            })
            task.resume()
        }
        else
        {
            let alert = UIAlertController(title: "Alert!", message: "No Internet Connection.", preferredStyle:.Alert)
            let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
            }
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func btnActionDownload(sender: UIButton)
    {
        let point = tblView.convertPoint(CGPoint.zero, fromView: sender)
        let indexPath:NSIndexPath = self.tblView.indexPathForRowAtPoint(point)!
        
        if contents.containsObject((self.fruitsList.objectForKey("data")?.objectAtIndex(indexPath.row).objectForKey("inv_pdf_filename"))!)
        {
            UIView.animateWithDuration(1.0,
                                       delay: 0.0,
                                       options: .CurveEaseInOut,
                                       animations: {
                                        self.subView.hidden = false
                                        self.lblInvName.text = (self.fruitsList.objectForKey("data")?.objectAtIndex(indexPath.row).objectForKey("inv_pdf_filename")) as? String
                                        
                                        self.subView.frame = CGRectMake(self.subView.frame.origin.x, 0, self.subView.frame.size.width, self.subView.frame.size.height)
                                        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
                                        let filePath = (String(format: "%@/%@",documentsPath,(self.fruitsList.objectForKey("data")?.objectAtIndex(indexPath.row).objectForKey("inv_pdf_filename")) as! NSString))
                                        let url = NSURL(fileURLWithPath: filePath)
                                        let urlRequest = NSURLRequest(URL: url)
                                        self.webView.loadRequest(urlRequest)
                                        
                },
                                       completion: { finished in
                                        
            })
        }
        else
        {
            startActivityIndicator("Downloading..")
            let url:NSURL = NSURL(string:(String(format: "http://mms.virtesting.net/upload/invoice_pdf/%@",((((self.fruitsList.objectForKey("data") as! NSArray).objectAtIndex(indexPath.row) as! NSDictionary).objectForKey("inv_pdf_filename"))! as? String)!)))!
            
            let session = NSURLSession.sharedSession()
            let urlRequest = NSURLRequest(URL: url)
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: {(data, response, error) in
                if let error = error {
                    print(error)
                }
                if let data = data{
                    print("data =\(data)")
                }
                if let response = response {
                    // print("url = \(response.URL!)")
                    print("response = \(response)")
                    // let httpResponse = response as! NSHTTPURLResponse
                    // print("response code = \(httpResponse.statusCode)")
                    
                    //if you response is json do the following
                    do{
                        //Get the local docs directory and append your local filename.
                        var docURL = (NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)).last
                        
                        docURL = docURL?.URLByAppendingPathComponent((((self.fruitsList.objectForKey("data") as! NSArray).objectAtIndex(indexPath.row) as! NSDictionary).objectForKey("inv_pdf_filename"))! as! String)
                        
                        //Lastly, write your file to the disk.
                        data?.writeToURL(docURL!, atomically: true)
                        dispatch_async(dispatch_get_main_queue(),{
                            self.stopActivityIndicator()
                            let alert = UIAlertController(title: "Alert!", message: "Download completed.", preferredStyle:.Alert)
                            let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                            }
                            alert.addAction(cancelAction)
                            self.contents.removeAllObjects()
                            self.fetchDocDir()
                            self.tblView.reloadData()
                            self.presentViewController(alert, animated: true, completion: nil)
                        })
                        
                    }
                }
            })
            task.resume()
        }
        
        
        /*Create image from PDF
         let pdf: CGPDFDocumentRef = CGPDFDocumentCreateWithURL(url)!
         var page: CGPDFPageRef;
         var frame: CGRect = CGRectMake(0, 0, 100, 200)
         let pageCount: Int = CGPDFDocumentGetNumberOfPages(pdf);
         for i in (0 ..< pageCount)
         {
         let mypage: CGPDFPageRef = CGPDFDocumentGetPage(pdf, i+1)!
         frame = CGPDFPageGetBoxRect(mypage, .MediaBox)
         UIGraphicsBeginImageContext(CGSizeMake(600, 600*(frame.size.height/frame.size.width)))
         let ctx: CGContextRef = UIGraphicsGetCurrentContext()!
         CGContextSaveGState(ctx)
         CGContextTranslateCTM(ctx, 0.0, frame.size.height)
         CGContextScaleCTM(ctx, 1.0, -1.0)
         CGContextSetGrayFillColor(ctx, 1.0, 1.0)
         CGContextFillRect(ctx, frame)
         page = CGPDFDocumentGetPage(pdf, i + 1)!
         let pdfTransform: CGAffineTransform = CGPDFPageGetDrawingTransform(page, .MediaBox, frame, 0, true)
         CGContextConcatCTM(ctx, pdfTransform);
         CGContextSetInterpolationQuality(ctx, .Default)
         CGContextSetRenderingIntent(ctx, .RenderingIntentDefault)
         CGContextDrawPDFPage(ctx, page)
         let thumbnailImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
         CGContextRestoreGState(ctx)
         UIImageWriteToSavedPhotosAlbum(thumbnailImage, nil, nil, nil);
         } */
    }
    
    func fetchDocDir()  {
        //Getting a list of the docs directory
        let docURL = (NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).last)
        
        //put the contents in an array.
        let data:NSArray = try! (NSFileManager.defaultManager().contentsOfDirectoryAtURL(docURL!, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions.SkipsHiddenFiles))
        
        for i in (0 ..< data.count) {
            let temp:NSString = data[i].absoluteString
            let theFileName = temp.lastPathComponent
            
            contents.addObject(theFileName)
        }
        
        //print the file listing to the console
        print(contents)
    }
    
    //MARK: - MBProgressHud Methods
    func startActivityIndicator(text: NSString) {
        
        let spinnerActivity = MBProgressHUD.showHUDAddedTo(self.view, animated: true);
        
        spinnerActivity.label.text = text as String;
                
        spinnerActivity.userInteractionEnabled = false;
        
    }
    
    func stopActivityIndicator() {
        MBProgressHUD.hideHUDForView(self.view, animated: true);
        
    }
}
