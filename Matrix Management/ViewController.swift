//
//  ViewController.swift
//  Matrix Management
//
//  Created by NetPyx on 20/04/17.
//  Copyright © 2017 NetPyx. All rights reserved.
//

import UIKit
import Foundation

var globalUrl : String = String("http://mms.virtesting.net/api/")

class ViewController: UIViewController {
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var imgUsername: UIImageView!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            imgUsername.layer.cornerRadius = 10.0;
            imgPassword.layer.cornerRadius = 10.0;
            btnLogin.layer.cornerRadius = 10.0;
        }
        else
        {
            imgUsername.layer.cornerRadius = 5.0;
            imgPassword.layer.cornerRadius = 5.0;
            btnLogin.layer.cornerRadius = 5.0;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Button Actions
    @IBAction func btnActionLogin(sender: AnyObject)
    {
        txtUsername.resignFirstResponder()
        txtPassword.resignFirstResponder()
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        if ReachabilityManager.sharedInstance.isConnectedToNetwork()
        {
            if (txtUsername.text == "")
            {
                let alert = UIAlertController(title: "Alert!", message: "Please enter username.", preferredStyle:.Alert)
                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                    print(action)
                }
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else if(txtPassword.text == "")
            {
                let alert = UIAlertController(title: "Alert!", message: "Please enter password.", preferredStyle:.Alert)
                let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                    print(action)
                }
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                startActivityIndicator()
                let session = NSURLSession.sharedSession()
                let request = NSMutableURLRequest(URL: NSURL(string: globalUrl)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
                
                // create some JSON data and configure the request
                let jsonString = String(format: "uname=%@&pwd=%@&api_type=login_via_api", txtUsername.text!, txtPassword.text!)
                request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
                request.HTTPMethod = "POST"
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                
                let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
                    if let error = error {
                        print(error)
                        self.stopActivityIndicator()
                        let alert = UIAlertController(title: "Alert!", message: error.localizedDescription, preferredStyle:.Alert)
                        let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                            print(action)
                        }
                        alert.addAction(cancelAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    if let data = data{
                        print("data =\(data)")
                    }
                    if let response = response {
                        // print("url = \(response.URL!)")
                        print("response = \(response)")
                        // let httpResponse = response as! NSHTTPURLResponse
                        // print("response code = \(httpResponse.statusCode)")
                        
                        //if you response is json do the following
                        do{
                            let resultJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions())
                            let arrayJSON = resultJSON as! NSDictionary
                            
                            if(arrayJSON.objectForKey("type") as! String == "success")
                            {
                                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "Is_Login")
                                NSUserDefaults.standardUserDefaults().setValue(arrayJSON.objectForKey("user_id"), forKey: "User_Id")
                                NSUserDefaults.standardUserDefaults().setValue(arrayJSON.objectForKey("user_name"), forKey: "User_Name")
                                dispatch_async(dispatch_get_main_queue(),{
                                    self.stopActivityIndicator()
                                    self.txtUsername.text = ""
                                    self.txtPassword.text = ""
                                    self.performSegueWithIdentifier("PushToHomeFromLogin", sender: self)
                                })
                            }
                            else
                            {
                                dispatch_async(dispatch_get_main_queue(),{
                                    self.stopActivityIndicator()
                                    let alert = UIAlertController(title: "Alert!", message: arrayJSON.objectForKey("message") as? String, preferredStyle:.Alert)
                                    let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                                        print(action)
                                    }
                                    alert.addAction(cancelAction)
                                    self.presentViewController(alert, animated: true, completion: nil)
                                })
                            }
                            
                        }catch _{
                            print("Received not-well-formatted JSON")
                        }
                    }
                })
                task.resume()
            }
        }
        else
        {
            let alert = UIAlertController(title: "Alert!", message: "No Internet Connection.", preferredStyle:.Alert)
            let cancelAction = UIAlertAction(title: "OK", style: .Cancel) { action in
                print(action)
            }
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    //MARK: - - UITextFieldDelegate
    func textFieldDidBeginEditing(textField: UITextField!) {
        if (textField == txtUsername)
        {
            self.view.frame = CGRectMake(self.view.frame.origin.x, -50, self.view.frame.size.width, self.view.frame.size.height);
        }
        else if (textField == txtPassword)
        {
            self.view.frame = CGRectMake(self.view.frame.origin.x, -100, self.view.frame.size.width, self.view.frame.size.height);
        }
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        if (textField == txtUsername)
        {
            txtPassword.becomeFirstResponder()
        }
        else if (textField == txtPassword)
        {
            txtPassword.resignFirstResponder()
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
        
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        txtUsername.resignFirstResponder()
        txtPassword.resignFirstResponder()
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    //MARK: - MBProgressHud Methods
    func startActivityIndicator() {
        
        let spinnerActivity = MBProgressHUD.showHUDAddedTo(self.view, animated: true);
        
        spinnerActivity.label.text = "Logging..";
                
        spinnerActivity.userInteractionEnabled = false;
        
    }
    
    func stopActivityIndicator() {
        MBProgressHUD.hideHUDForView(self.view, animated: true);
        
    }
}

